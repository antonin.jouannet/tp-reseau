###### tags: `RESEAU`
B2 | TP3 réseau leo (27/09/2021 - )

# RESEAU TP3 : Progressons vers le réseau d'infrastructure

# I. (mini)Architecture réseau

## 1. Adressage

🌞 Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme :


| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`      |`255.255.255.192 /26`| 64                          | `10.3.0.126`       | `10.3.0.127`                                                                                 |
| `server1`     | `10.3.0.0`        |`255.255.255.128 /25`| 128                         | `10.3.0.190`       | `10.3.0.191`                                                                                 |
| `server2`     | `10.3.0.192`      |`255.255.255.240 /28`| 16                          | `10.3.0.206`       | `10.3.0.207`                                                                                 |


🌞 Vous remplirez aussi au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, le 🗃️ tableau d'adressage 🗃️ suivant :

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.?.?/?`         | `10.3.?.?/?`         | `10.3.?.?/?`         | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |


## 2. Routeur
🖥️ VM router.tp3


🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle



- il a un accès internet

```
[antonin@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=25.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=22.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=22.4 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 22.417/23.640/25.891/1.603 ms
[antonin@router ~]$
```

- il a de la résolution de noms
```
[antonin@router ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xmv-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=23.4 ms
64 bytes from xmv-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=22.9 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 22.927/23.181/23.436/0.296 ms
[antonin@router ~]$
```

- il porte le nom router.tp3*

```
[antonin@router ~]$ hostname
router.tp3
[antonin@router ~]$

```

- n'oubliez pas d'activer le routage sur la machine

```
[antonin@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent

[sudo] password for antonin:
success
[antonin@router ~]$
```

---

# II. Services d'infra

## 1. Serveur DHCP
🖥️ VM dhcp.client1.tp3

🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra :

- porter le nom dhcp.client1.tp3

```
[antonin@dhcp ~]$ hostname
dhcp.client1.tp3
[antonin@dhcp ~]$
```

- 📝checklist📝

- donner une IP aux machines clients qui le demande

- leur donner l'adresse de leur passerelle

- leur donner l'adresse d'un DNS utilisable

📁 Fichier dhcpd.conf

Pour pouvoir mettre en place le serveur DHCP je me rends dans le fihcier dhcpd.conf pourl e modifier```/etc/dhcp/dhcpd.conf``` et je fait la chose suivante : 

```
[antonin@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
subnet 10.3.0.128 netmask 255.255.255.192 {
    range 10.3.0.131 10.3.0.189;
    option domain-name-servers 1.1.1.1;
    option routers 10.3.0.190;
}

[antonin@dhcp ~]$
```

🖥️ VM marcel.client1.tp3
🌞 Mettre en place un client dans le réseau client1

de son p'tit nom marcel.client1.tp3

```
[antonin@marcel ~]$ hostname
marcel.client1.tp3
[antonin@marcel ~]$
```

- 📝checklist📝
- la machine récupérera une IP dynamiquement grâce au serveur DHCP
- ainsi que sa passerelle et une adresse d'un DNS utilisable

```
[antonin@dhcp ~]$ sudo systemctl enable --now dhcpd (erreur)
[antonin@dhcp ~]$ sudo firewall-cmd --add-service=dhcp
Warning: ALREADY_ENABLED: 'dhcp' already in 'public'
success
[antonin@dhcp ~]$ sudo firewall-cmd --runtime-to-permanent
[sudo] password for antonin:
success
[antonin@dhcp ~]$
```

[ps] : j'ai dû avancer sur mon tp linux et j'ai pris énormément de retard sur le réseau, j'ai demandé à pas mal de gens en ce qui concernait mon erreur et j'ai testé pas mal de choses avec ce que je trouvais sur internet mais rian a faire.. J'ai regardé la suite du tp et je pense que je peux aller relativement vite si j'arrive à débloquer ça : 

```
[antonin@dhcp ~]$ sudo systemctl enable --now dhcpd
Failed to enable unit: Unit file /usr/lib/systemd/system/dhcpd.service is masked.
[antonin@dhcp ~]$
```

je vais regler ce problème au plus vite et tracer jusqu'a la fin

🌞 Depuis marcel.client1.tp3

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP


- à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau

