###### tags: `LINUX`
#B2 - TP LEO 
# LINUX TP2 : TP2 pt. 1 : Gestion de service

# I. Un premier serveur web

## 1. Installation


| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | ?             |


🌞 Installer le serveur Apache

- paquet httpd

Compilation et installation : ```sudo yum install httpd```


- la conf se trouve dans /etc/httpd/

- le fichier de conf principal est /etc/httpd/conf/httpd.conf

- je vous conseille vivement de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair

  - avec vim vous pouvez tout virer avec :g/^ *#.*/d


🌞 Démarrer le service Apache

- le service s'appelle httpd (raccourci pour httpd.service en réalité)

- démarrez le

Je démarre mon service Apache encommencant par rentrer la commande **__sudo systemctl start httpd__** puis je vérifie qu'il est effectivement bien actif avec la commande **__sudo systemctl status httpd__**
```
[antonin@web ~]$ sudo systemctl start httpd
[antonin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset>   Active: active (running) since Wed 2021-10-06 10:13:30 CEST; 43s ago
 Docs: man:httpd.service(8)
Main PID: 1497 (httpd)
Status: "Running, listening on: port 80"
Tasks: 213 (limit: 11398)
Memory: 29.1M
CGroup: /system.slice/httpd.service
       ├─1497 /usr/sbin/httpd -DFOREGROUND
       ├─1498 /usr/sbin/httpd -DFOREGROUND
       ├─1499 /usr/sbin/httpd -DFOREGROUND
       ├─1500 /usr/sbin/httpd -DFOREGROUND
       └─1501 /usr/sbin/httpd -DFOREGROUND

Oct 06 10:13:30 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 10:13:30 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 10:13:30 web.tp2.linux httpd[1497]: Server configured, listening on: port >lines 1-18/18 (END)
```

- faites en sorte qu'Apache démarre automatique au démarrage de la machine

Commande me permettant de run mon service a chaque redémarrage de la machine, donc j'exécute la commande, je reboot et ça fonctionne

```
[antonin@web ~]$ sudo systemctl enable httpd
[sudo] password for antonin:
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[antonin@web ~]$ 
```

après avoir reboot : 

```
PS C:\Users\anton> ssh antonin@10.102.1.11
antonin@10.102.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Oct  6 11:19:11 2021
[antonin@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset:>   Active: active (running) since Wed 2021-10-06 11:19:04 CEST; 34s ago
 Docs: man:httpd.service(8)
Main PID: 832 (httpd)
Status: "Running, listening on: port 80"
Tasks: 213 (limit: 11398)
Memory: 31.2M

[...]

Oct 06 11:19:04 web.tp2.linux httpd[832]: Server configured, listening on: port 80
```


- ouvrez le port firewall nécessaire

J'ouvre le port firewall avec les commandes suivantes : 

Ouverture du port
````
[antonin@web ~]$  sudo firewall-cmd --add-port=80/tcp --permanent
success
[antonin@web ~]$
````

reload du firewall
```
[antonin@web ~]$sudo firewall-cmd --reload
success
[antonin@web ~]$
```
vérification
```
[antonin@web ~]$ sudo firewall-cmd --list-ports
80/tcp
[antonin@web ~]$ 
```


- utiliser une commande ss pour savoir sur quel port tourne actuellement Apache

Apache tourne sur le port 80

```
[antonin@web ~]$ sudo firewall-cmd --list-ports
80/tcp
[antonin@web ~]$ 
```

🌞 TEST

- vérifier qu'il est configuré pour démarrer automatiquement


- vérifier avec une commande curl localhost que vous joignez votre serveur web localement

```
[antonin@web ~]$ curl localhost
<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<title>HTTP Server Test Page powered by: Rocky Linux</title>
<style type="text/css">
  /*<![CDATA[*/

[...]

<footer class="col-sm-12">
  <a href="https://apache.org">Apache&trade;</a> is a registered trademark of
<a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
  <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
  </footer>

</body>
</html>

```

- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web



## 2. Avancer vers la maîtrise du service

🌞 Le service Apache...

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

**__sudo systemctl enable httpd__**

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume

après avoir fait la commande **__systemctl status httpd__** voici la ligne qui confirme que le service est paramétré pour démarrer quand la machine s'allume : 

```
[antonin@web home]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled;
```

- affichez le contenu du fichier httpd.service qui contient la définition du service Apache

```
[antonin@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
[antonin@web ~]$
```

🌞 Déterminer sous quel utilisateur tourne le processus Apache

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (httpd.conf) qui définit quel user est utilisé

On rentre dans le fichier de conf : **__cd /etc/httpd/conf__**

et on fait un **__cat httpd.conf | grep User__** pour afficher le user : 

```
[antonin@web conf]$ cat httpd.conf| grep User
User apache
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```


- utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

on fait un **__cat httpd.conf| grep User__** pour afficher le user : 

```
[antonin@web conf]$ cat httpd.conf| grep User
User apache
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
  LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```

- la page d'accueil d'Apache se trouve dans /usr/share/testpage/

    - vérifiez avec un ls -al que tout son contenu est accessible en lecture à l'utilisateur mentionné dans le fichier de conf

```
[antonin@web ~]$ ls -larth /var/www/
total 4.0K
drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
drwxr-xr-x. 22 root root 4.0K Sep 29 15:25 ..
drwxr-xr-x.  4 root root   33 Sep 29 15:25 .
[antonin@web ~]$ 
```

🌞 Changer l'utilisateur utilisé par Apache

- créez le nouvel utilisateur

- pour les options de création, inspirez-vous de l'utilisateur Apache existant

    - le fichier /etc/passwd contient les informations relatives aux utilisateurs existants sur la machine
        
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut

nouvel utilisateur 
```
[antonin@web ~]$ sudo useradd weblinux -m -d /usr/share/httpd -s /sbin/nologin
[sudo] password for antonin:
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
[antonin@web ~]$ 
```


- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

```
[antonin@web conf]$ cat /etc/httpd/conf/httpd.conf | grep "User "
User weblinux
[antonin@web conf]$
```

- redémarrez Apache

```
[antonin@web conf]$ sudo systemctl restart httpd
```


- utilisez une commande ps pour vérifier que le changement a pris 

```
[antonin@web conf]$ ps -ef | grep httpd
root        3316       1  0 13:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
weblinux    3318    3316  0 13:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
weblinux    3319    3316  0 13:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
weblinux    3320    3316  0 13:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
weblinux    3321    3316  0 13:37 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
antonin     3543    1706  0 13:39 pts/0    00:00:00 grep --color=auto httpd 
[antonin@web conf]$
```

🌞 Faites en sorte que Apache tourne sur un autre port

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix

Je modifie le fichier httpd.conf et choisi le port de mon choix : 

```
[antonin@web conf]$ cat /etc/httpd/conf/httpd.conf | grep Listen
#Listen 12.34.56.78:80
Listen 800
```

- ouvrez un nouveau port firewall, et fermez l'ancien

```
-s[antonin@web conf]$ sudo firewall-cmd --add-port=54346/tcp --permanent
success
[antonin@web conf]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[antonin@web conf]$ sudo firewall-cmd --reload
success
```

- redémarrez Apache

```
[antonin@web conf]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-10-08 13:55:47 CEST; 6s ago
     Docs: man:httpd.service(8)
 Main PID: 3779 (httpd)

[...]

Oct 08 13:55:47 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 08 13:55:47 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 08 13:55:47 web.tp2.linux httpd[3779]: Server configured, listening on: port 54346
[antonin@web conf]$ 
```

- prouvez avec une commande ss que Apache tourne bien sur le nouveau port choisi

```
[antonin@web conf]$ sudo ss -antpl | grep httpd
LISTEN 0      128                *:54346            *:*    users:(("httpd",pid=3783,fd=4),("httpd",pid=3782,fd=4),("httpd",pid=3781,fd=4),("httpd",pid=3779,fd=4))
```

- vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port

```
[antonin@web conf]$ curl localhost:54346 | wc -l
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  1240k      0 --:--:-- --:--:-- --:--:-- 1240k
308
[antonin@web conf]$
```

- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

```
PS C:\Users\anton> curl 10.102.1.11:54346
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you
can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page you've
expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the website's domain
should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"

[...]

Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
Au caractère Ligne:1 : 1
+ curl 10.102.1.11:54346
+ ~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation : (System.Net.HttpWebRequest:HttpWebRequest) [Invoke-WebRequest], WebException
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeWebRequestCommand
```

---

# II. Une stack web plus avancée

## 2. Setup

## A. Serveur Web et NextCloud
Créez les 2 machines et déroulez la 📝checklist📝.
🌞 Install du serveur Web et de NextCloud sur web.tp2.linux

- n'oubliez pas de réinitialiser votre conf Apache avant de continuer
    - remettez le port et le user par défaut en particulier
```
[antonin@web ~]$ cat /etc/httpd/conf/httpd.conf
#
ServerRoot "/etc/httpd"

#Listen 12.34.56.78:80
Listen 80

#
Include conf.modules.d/*.conf

#
User apache
Group apache

#
ServerAdmin root@localhost

[...]

IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*
[antonin@web ~]$ 
```

- déroulez la doc d'install de Rocky
    - uniquement pour le serveur Web + NextCloud, vous ferez la base de données MariaDB après
    - quand ils parlent de la base de données, juste vous sautez l'étape, on le fait après :)


je veux dans le rendu toutes les commandes réalisées
n'oubliez pas la commande history qui permet de voir toutes les commandes tapées précédemment

__Commandes Nextcloud__

```
- dnf install epel-release
- dnf update
- dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
- dnf module list php
- dnf module enable php:remi-7.4
- dnf module list php
- dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype 
php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml 
php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session 
php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd 
php74-php-intl php74-php-bcmath php74-php-gmp
- systemctl enable httpd
- vim /etc/httpd/sites-available/web.tp2.linux
- ln -s /etc/httpd/sites-available/web.tp2.linuw /etc/httpd/sites-enabled/
- mkdir -p /var/www/sub-domains/com.yourdomain.com/html
- cd /usr/share/zoneinfo
- timedatectl
- ls -al /etc/localtime 
- wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
- unzip nextcloud-21.0.1.zip
- cd nextcloud/
- cp -Rf * /var/www/sub-domains/web.tp2.linuxa/html/
- chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html
- mv /var/www/sub-domains/web.tp2.linux/html/data /var/www/sub-domains/web.tp2.linux/
```

Une fois que vous avez la page d'accueil de NextCloud sous les yeux avec votre navigateur Web, NE VOUS CONNECTEZ PAS et continuez le TP

📁 Fichier __```/etc/httpd/conf/httpd.conf```__ (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)
📁 Fichier __```/etc/httpd/sites-available/web.tp2.linux```__

---

## B. Base de données
🌞 Install de MariaDB sur db.tp2.linux

- déroulez la doc d'install de Rocky

- manipulation

- je veux dans le rendu toutes les commandes réalisées

Commandes Mariadb

```
- sudo dnf install mariadb-server
- sudo systemctl enable mariadb
- sudo systemctl start mariadb
- mysql_secure_installation
```

- vous repérerez le port utilisé par MariaDB avec une commande ```ss``` exécutée sur db.tp2.linux

```
[antonin@db ~]$ sudo ss -alnpt
[sudo] password for antonin:
State   Recv-Q  Send-Q   Local Address:Port    Peer Address:Port  Process
LISTEN  0       128            0.0.0.0:22           0.0.0.0:*      users:(("sshd",pid=832,fd=5))
-> LISTEN  0       80                   *:3306               *:*      users:(("mysqld",pid=26268,fd=21))
LISTEN  0       128               [::]:22              [::]:*      users:(("sshd",pid=832,fd=7))
[antonin@db ~]$ 
```

🌞 Préparation de la base pour NextCloud

- une fois en place, il va falloir préparer une base de données pour NextCloud :
    - connectez-vous à la base de données à l'aide de la commande sudo mysql -u root -p

```
[antonin@db ~]$  sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 21
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>

```

- exécutez les commandes SQL suivantes : 

```
- CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
- CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
- GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
- FLUSH PRIVILEGES;
```

🌞 Exploration de la base de données

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :

    - depuis la machine web.tp2.linux vers l'IP de db.tp2.linux

    - vous pouvez utiliser la commande mysql pour vous connecter à une base de données depuis la ligne de commande

        - par exemple ```mysql -u <USER> -h <IP_DATABASE> -p```

utilisez les commandes SQL fournies ci-dessous pour explorer la base : 

> SHOW DATABASES;

```
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
4 rows in set (0.001 sec)

MariaDB [(none)]>

```

> USE <DATABASE_NAME>;
```
MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]>

```

> SHOW TABLES;
```
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)

MariaDB [nextcloud]> 
```

trouver une commande qui permet de lister tous les utilisateurs de la base de données

```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+-------------+
| User      | Host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.002 sec)

MariaDB [(none)]>
```
---

## C. Finaliser l'installation de NextCloud
🌞 sur votre PC

- modifiez votre fichier hosts (oui, celui de votre PC, de votre hôte)

    - pour pouvoir joindre l'IP de la VM en utilisant le nom web.tp2.linux


- avec un navigateur, visitez NextCloud à l'URL http://web.tp2.linux

    - c'est possible grâce à la modification de votre fichier hosts

Mon fichier host ce trouve dans ```C:\Windows\System32\drivers\etc\hosts```

>J'ai ajouté la dernière ligne
```
#
127.0.0.1 localhost
::1 localhost
10.102.1.11 web web.tp2.linux

```

- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
    - ne saisissez rien pour le moment

- cliquez sur "Storage & Database" juste en dessous
    - choisissez "MySQL/MariaDB"
    - saisissez les informations pour que NextCloud puisse se connecter avec votre base

- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation


🌞 Exploration de la base de données

- connectez vous en ligne de commande à la base de données après l'installation terminée


- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation


    - bonus points si la réponse à cette question est automatiquement donnée par une requête SQL


---

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80 /tcp          | *             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306/tcp           | 10.102.1.11             |
