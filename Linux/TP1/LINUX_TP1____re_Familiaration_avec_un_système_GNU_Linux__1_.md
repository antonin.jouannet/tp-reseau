#B2 - TP LEO 
# LINUX TP1 : (re)Familiaration avec un système GNU/Linux

---

## 0. Préparation de la machine

🌞 Setup de deux machines Rocky Linux configurée de façon basique.

- un accès internet

  - carte réseau dédiée
  - route par défaut

Pour prouver que j'ai un accès à Internet sur mes deux machines je vais ping google en utilisant le nom de domaine (google.com) mais aussi son ip (8.8.8.8) : 

> #### machine 1

```
[antonin@vm1 ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=115 time=24.2 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=2 ttl=115 time=24.0 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=3 ttl=115 time=28.5 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=4 ttl=115 time=26.6 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 24.024/25.833/28.474/1.835 ms
```

```
[antonin@vm1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=29.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=24.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=23.0 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=115 time=25.5 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 23.041/25.686/29.984/2.635 ms
```


> #### machine 2

```
[antonin@vm2 ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=115 time=24.7 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=2 ttl=115 time=27.4 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=3 ttl=115 time=28.3 ms
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=4 ttl=115 time=27.1 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 24.663/26.873/28.340/1.356 ms
```

```
[antonin@vm2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=26.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=33.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=25.1 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=115 time=21.9 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 21.949/26.730/33.411/4.188 ms
```

- un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only)

  - carte réseau dédiée (host-only sur VirtualBox)
  - les machines doivent posséder une IP statique sur l'interface host-only

> #### ping de ma vm1 vers ma vm2
```
[antonin@vm1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.15 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.676 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.710 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.717 ms
^C
--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3047ms
rtt min/avg/max/mdev = 0.676/0.812/1.146/0.194 ms
```

> #### ping de ma vm2 vers ma vm1

```
[antonin@vm2 ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.670 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.761 ms
64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=0.631 ms
64 bytes from 10.101.1.11: icmp_seq=4 ttl=64 time=0.757 ms
^C
--- 10.101.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3088ms
rtt min/avg/max/mdev = 0.631/0.704/0.761/0.064 ms
```

- vous n'utilisez QUE ssh pour administrer les machines



- les machines doivent avoir un nom

- utiliser **1.1.1.1** comme serveur DNS

Pour utiliser **1.1.1.1** comme serveur DNS, j'utilise la commande **__/etc/sysconfig/network-scripts/ifcfg-enp0s8__** et j'ajoute le dns comme montré ci-dessous sur mes deux machines

#### vm1 : 

```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0

DNS1=1.1.1.1

```


#### vm2 

```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0

DNS1=1.1.1.1
```

 - vérifier avec le bon fonctionnement avec la commande dig

  
   - avec dig, demander une résolution du nom ynov.com

maintenant on vérifie que ça fonctionne bien en utilisant **_dig_**

#### vm1

```
[antonin@vm1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20300
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9064    IN      A       92.243.16.143

;; Query time: 15 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Wed Sep 22 14:38:19 CEST 2021
;; MSG SIZE  rcvd: 53
```

#### vm2

```
[antonin@vm2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32922
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9052    IN      A       92.243.16.143

;; Query time: 13 msec
;; SERVER: 192.168.1.1#53(192.168.1.1)
;; WHEN: Wed Sep 22 14:38:30 CEST 2021
;; MSG SIZE  rcvd: 53
```

   - mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé

#### vm1

```
ynov.com.               9064    IN      A       92.243.16.143
```

#### vm2

```
ynov.com.               9052    IN      A       92.243.16.143
```

   - mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu

Le même serveur me réponds sur les deux machines

```
;; SERVER: 192.168.1.1#53(192.168.1.1)
```

- les machines doivent pouvoir se joindre par leurs noms respectifs

  - fichier /etc/hosts


  - assurez-vous du bon fonctionnement avec des ```ping <NOM>```
  
- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires 

  - commande ```firewall-cmd```
  
---
  
## I. Utilisateurs
  
1. Création et configuration

🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :

- le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home

Je suis ici a la racine : ```[antonin@vm1 ~]$```, pour ajouter un nouvel utilisateur, je vais me placer dans /home/ puis je n'ai plus qu'a créer mon nouvel utilisateur avec la commande **__sudo useradd user -d /home/ -s /bin/bash -u 100** : 

```
[antonin@vm1 home]$ sudo useradd user -d /home/ -s /bin/bash -u 100
```

Voila un **__cat /etc/passwd__** pour montrer qu'il est bien la : 

```
[antonin@vm1 home]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin

[...]

antonin:x:1000:1000:Antonin:/home/antonin:/bin/bash
tcpdump:x:72:72::/:/sbin/nologin
user:x:100:1002::/home/:/bin/bash
```




- le shell de l'utilisateur soit /bin/bash


🌞 Créer un nouveau groupe ```admins``` qui contiendra les utilisateurs de la machine ayant accès aux droits de ```root``` via la commande **__sudo__**.

```
sudo groupadd admins
```

Pour vérifier que mon groupe est bien créé, je fais un **__cat /etc/group__**, mon groupe "admins" ce trouve tout à la fin : 

```
[antonin@vm1 home]$ cat /etc/group
root:x:0:
bin:x:1:

[...]

antonin:x:1000:
tcpdump:x:72:
admins:x:1001:
```


Pour permettre à ce groupe d'accéder aux droits ```root``` :

il faut modifier le fichier ```/etc/sudoers```

- on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur

- la commande **__visudo__** permet d'éditer le fichier, avec un check de syntaxe avant fermeture

- ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe wheel)

Je fais un **__visudo__** pour rentrer dans le fichier, et j'insère la ligne suivante pour donner a mon groupe "admins" les droits root : 
**__%admins ALL=(ALL) ALL__**

🌞 Ajouter votre utilisateur à ce groupe admins.

1. Utilisateur créé et configuré
2. Groupe admins créé
3. Groupe admins ajouté au fichier /etc/sudoers
4. Ajout de l'utilisateur au groupe admins

```
[antonin@vm1 home]$ sudo usermod -aG admins user
[antonin@vm1 home]$ groups user
user : user admins
[antonin@vm1 home]$
```

## 2. SSH

🌞 Pour cela :

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )

  - génération de clé depuis VOTRE poste donc
  - sur Windows, on peut le faire avec le programme ```puttygen.exe``` qui est livré avec ```putty.exe```

Je créé une clé ssh publique depuis mon pc en faisant : 

```
PS C:\Users\anton> ssh-keygen -t rsa -b 4096
```

- déposer la clé dans le fichier ```/home/<USER>/.ssh/authorized_keys``` de la machine que l'on souhaite administrer

  - vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP
  - on peut le faire à la main
  - ou avec la commande ```ssh-copy-id```


```
PS C:\Users\anton> ssh antonin@10.101.1.11
antonin@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 22:03:36 2021 from 10.101.1.1
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 22:03:36 2021 from 10.101.1.1
[antonin@vm1 ~]$ cat .ssh/authorized_keys
SHA256:U2TWDa271CT8lql/gwkN53GrV97v6mBHM1105971/sM
[antonin@vm1 ~]$ ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/home/antonin/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/antonin/.ssh/id_rsa.
Your public key has been saved in /home/antonin/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:vWXYDytQCXI/jK0CFD4K0RBhLl/TAlKS9yJSayj/q94 antonin@vm1.tp1.b2
The key's randomart image is:
+---[RSA 4096]----+
|BOo o.. o        |
|++++ . o * .     |
|o= oB . . B      |
|*o+o.=   + +     |
|o+o.  . S o =    |
|  .    . . + +   |
|   .      o . .  |
|   ..      .     |
| .o.E.           |
+----[SHA256]-----+
```

ssh-copy-id -i .ssh/id_rsa.pub antonin@10.101.1.11 me permet de déposer ma clé
```
[antonin@vm1 ~]$ ssh-copy-id -i .ssh/id_rsa.pub antonin@10.101.1.11
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: ".ssh/id_rsa.pub"
The authenticity of host '10.101.1.11 (10.101.1.11)' can't be established.
ECDSA key fingerprint is SHA256:KI/qSBwLbTOBdMbiK44F5ULNgnvPmnj/l4O6e5gkImM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
antonin@10.101.1.11's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'antonin@10.101.1.11'"
and check to make sure that only the key(s) you wanted were added.
```

🌞 Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.

```
PS C:\Users\anton> ssh antonin@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 22:06:58 2021 from 10.101.1.1
```
---

# II. Partitionnement

## 1. Préparation de la VM

Uniquement sur node1.tp1.b2.

Commençons tout d'abord par montrer tout les disques créés avec la commande **__lsblk__** : 

```
[antonin@vm1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```

Mes disques sont bien ajoutés

## 2. Partitionnement

Uniquement sur node1.tp1.b2.

🌞 Utilisez LVM pour : 


- agréger les deux disques en un seul volume group

> 1er volume group
```
[antonin@vm1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for antonin:
  Physical volume "/dev/sdb" successfully created.
[antonin@vm1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[antonin@vm1 ~]$
```

```
[antonin@vm1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
```

```
[antonin@vm1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
```

- créer 3 logical volumes de 1 Go chacun

```
[antonin@vm1 ~]$ sudo lvcreate -L 1G data -n LVTP1_N1
  Logical volume "LVTP1_N1" created.
[antonin@vm1 ~]$ sudo lvcreate -L 1G data -n LVTP1_N2
  Logical volume "LVTP1_N2" created.
[antonin@vm1 ~]$ sudo lvcreate -L 1G data -n LVTP1_N3
  Logical volume "LVTP1_N3" created.
[antonin@vm1 ~]$
```

- formater ces partitions en ext4

> Logical Volume 1

```
[antonin@vm1 ~]$ sudo mkfs -t ext4 /dev/data/LVTP1_N1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ae394d61-79c7-42b1-8f99-aaa2de39ae64
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

> Logical Volume 2

```
[antonin@vm1 ~]$ sudo mkfs -t ext4 /dev/data/LVTP1_N2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 1036f4fd-96aa-475b-84ac-48441aba9e63
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

> Logical Volume 3

```
[antonin@vm1 ~]$ sudo mkfs -t ext4 /dev/data/LVTP1_N3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 069d09ea-d5f1-4913-a9f0-a6735e6af4ed
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

- monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.



🌞 Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

---

# III. Gestion de services

## 1. Interaction avec un service existant

Uniquement sur node1.tp1.b2.

🌞 Assurez-vous que : 

- l'unité est démarrée

Grâce a cette commande, je sais que l'unité est active : 

```
[antonin@vm1 ~]$ systemctl is-active firewalld
active
[antonin@vm1 ~]$
```

- l'unitée est activée (elle se lance automatiquement au démarrage)

## 2. Création de service
Uniquement sur node1.tp1.b2.

#### A. Unité simpliste

🌞 Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.

Ouverture du port 8888
```
[antonin@vm1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[antonin@vm1 ~]$
```

Je me place maintenant dans le fichier /etc/systemd/system puis on créé un fichier nommé ```web.service``` : 

```
[antonin@vm1 ~]$ cd /etc/systemd/system/
[antonin@vm1 system]$ touch web.service
touch: cannot touch 'web.service': Permission denied
[antonin@vm1 system]$ sudo !!
sudo touch web.service
```

Je modifie le fichier ```web.service``` en faisant un **__sudo nano web.service__** :  

```
[antonin@vm1 system]$ cat web.service
[Unit] Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
[antonin@vm1 system]$
```

après on reload avec la commande **__sudo systemctl daemon-reload__**




🌞Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.


#### B. Modification de l'unité

🌞 Créer un utilisateur web.

```
[antonin@vm1 system]$ sudo useradd web -m -s /bin/bash
```

🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses :


- User= afin de lancer le serveur avec l'utilisateur web dédié

- WorkingDirectory= afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans /srv

- ces deux clauses sont à positionner dans la section [Service] de votre unité

🌞 Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.


🌞 Vérifier le bon fonctionnement avec une commande curl