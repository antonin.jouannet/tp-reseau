B2 | TP1 réseau leo (13/09/2021 - 20/09/2021)

Ce tp à été réalisé avec Mukesh, Valentin et Sacha pour les parties ou nous devions être à plusieurs.

# RESEAU TP1

# I. Exploration locale en solo

---

## 1. Affichage d'informations sur la pile TCP/IP locale
> - En ligne de commande

🌞 Affichez les infos des cartes réseau de votre PC

J'affiche les informations des cartes réseau de mon pc en utilisant la commande **_ipconfig /all_**. Comme ça, je peux avoir toutes les données qui m'intéressent.

Ici, j'ai sélectionné la carte réseau Wi-Fi ou est donc indiqué son nom, son adresse MAC, son IP.
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 5C-3A-45-06-7A-5F
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b1da:c56d:bcf4:d995%16(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.250(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 11:11:28
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 13:14:24
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 89930309
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-72-CB-D2-00-E0-4C-68-A2-A1
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

Et pour la carte Ethernet, je n'en ai pas car mon pc ne possède pas de port Ethernet.

🌞 Affichez votre gateway

J'utilise la commande **_ipconfig /all_**
 pour afficher les informations de mes cartes, dans ùa carte réseau Wi-Fi, on peut trouver la ligne suivante qui correspond a la passerelle : 
 
 ```
 Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
> - En graphique (GUI : Graphical User Interface)
> 
Pour trouver l'adresse IP, l'adresse MAC et la gateway de ma carte Wi-Fi, je vais devoir faire plusieurs manipulations. 
Tout d'abord je clique droit sur l'icône de connexion dans la barre de tâche

![](https://i.imgur.com/lPsv7oN.png)

puis je sélectionne **_ouvrir les paramètres de réseau et Internet_**

![](https://i.imgur.com/d9PX1Yz.png)

une page s'affiche

![](https://i.imgur.com/6O1Nbis.png)

je sélectionne ensuite dans la partie **_modifier vos paramètres réseaux_**, la partie **_Afficher vos propriétés réseau_**

![](https://i.imgur.com/qKPtikf.png)

Je tombe ensuite sur toute les données dont j'ai besoin, je vais y retrouver l'adresse IP, l'adresse MAC et la gateway de ma carte Wi-Fi

![](https://i.imgur.com/uy8k4ta.png)





- Questions

🌞 à quoi sert la gateway dans le réseau d'YNOV ?

---

## 2. Modifications des informations

> - A. Modification d'adresse IP (part 1)

🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
(changez l'adresse IP de votre carte WiFi pour une autre
ne changez que le dernier octet)

Pour changer l'adresse IP de ma carte réseau, je vais lister les étapes à suivre : 

1) On commence par aller dans le panneau de contrôle en tapant dans la barre de recherche **_panneau de configuration_**.

![](https://i.imgur.com/bYg6cw0.png)

2) Une fois dedans, on clique sur **Réseau et Internet**

![](https://i.imgur.com/2zI0GPo.png)

3) Ensuite **_Centre réseau et Partage_**


![](https://i.imgur.com/0d0zfGt.png)

4) Ensuite on clique sur **_Modifier les paramètres de la carte_**

![](https://i.imgur.com/pojf30f.png)


5) Après on double clique sur la carte Wi-Fi

![](https://i.imgur.com/Db1rMcK.png)

6) Ensuite on va sur **_propriétés_**

![](https://i.imgur.com/VHfhTBX.png)

7) On défile dans le menu jusqu'a tomber sur l'endroit ou est inscrit **_Protocole Internet version 4 (TCP/IPv4)_**

![](https://i.imgur.com/lwiBvIZ.png)

8) On clique sur **_propriétés_**

![](https://i.imgur.com/vlHQijM.png)

9) Je change maintenant mon adresse IP en sélectionnant **_Utiliser l'adresse IP suivante_**, je ne change que le dernier octet de mon adresse. Et effectivement je n'ai plus accès à Internet.

![](https://i.imgur.com/WHumw7P.png)


🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

J'ai bien perdu l'accès à Internet. Selon moi, c'est parce que l'adresse IP que j'ai mise est certainement déja attribuée sur le réseau.


> - B. Table ARP

🌞 Exploration de la table ARP


- depuis la ligne de commande, afficher la table ARP
 
La table ARP s'affiche en utilisant la commande **_arp -a_**

```
Interface : 192.168.56.1 --- 0x3
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.10.1 --- 0xd
  Adresse Internet      Adresse physique      Type
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.70.1 --- 0xe
  Adresse Internet      Adresse physique      Type
  192.168.70.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.1.54 --- 0x10
  Adresse Internet      Adresse physique      Type
  192.168.1.1           60-35-c0-aa-e5-23     dynamique
  192.168.1.36          f4-f5-e8-16-9f-c2     dynamique
  192.168.1.207         ec-b5-fa-30-45-8b     dynamique
  192.168.1.215         a4-77-33-ae-fe-ed     dynamique
  192.168.1.236         60-35-c0-ec-57-0c     dynamique
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.120.1 --- 0x13
  Adresse Internet      Adresse physique      Type
  192.168.120.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.20.1 --- 0x18
  Adresse Internet      Adresse physique      Type
  10.10.20.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.245.1 --- 0x1a
  Adresse Internet      Adresse physique      Type
  192.168.245.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.250           01-00-5e-00-00-fa     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

- identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

.....

🌞 Et si on remplissait un peu la table ?

- envoyez des ping vers des IP du même réseau que vous. Lesquelles ? menfou, random. Envoyez des ping vers au moins 3-4 machines

J'envoie des pings sur quelques IP du réseau : 

**1ère machine**
```
PS C:\Users\anton> ping 10.33.2.250

Envoi d’une requête 'Ping'  10.33.2.250 avec 32 octets de données :
Réponse de 10.33.2.250 : octets=32 temps<1ms TTL=128
Réponse de 10.33.2.250 : octets=32 temps<1ms TTL=128
Réponse de 10.33.2.250 : octets=32 temps<1ms TTL=128
Réponse de 10.33.2.250 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 10.33.2.250:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

**2ème machine**
```
PS C:\Users\anton> ping 10.33.3.232

Envoi d’une requête 'Ping'  10.33.3.232 avec 32 octets de données :
Réponse de 10.33.3.232 : octets=32 temps=295 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=857 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=673 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=4 ms TTL=64

Statistiques Ping pour 10.33.3.232:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 4ms, Maximum = 857ms, Moyenne = 457ms
```

**3ème machine**
```
PS C:\Users\anton> ping 10.33.3.218

Envoi d’une requête 'Ping'  10.33.3.218 avec 32 octets de données :
Réponse de 10.33.3.218 : octets=32 temps=1724 ms TTL=64
Réponse de 10.33.3.218 : octets=32 temps=1003 ms TTL=64
Réponse de 10.33.3.218 : octets=32 temps=17 ms TTL=64
Réponse de 10.33.3.218 : octets=32 temps=6 ms TTL=64

Statistiques Ping pour 10.33.3.218:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 6ms, Maximum = 1724ms, Moyenne = 687ms
```


- affichez votre table ARP

J'affiche ma table arp avec la commande **_arp -a_**
```
PS C:\Users\anton> arp -a

Interface : 192.168.56.1 --- 0x3
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.10.1 --- 0xd
  Adresse Internet      Adresse physique      Type
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.70.1 --- 0xe
  Adresse Internet      Adresse physique      Type
  192.168.70.254        00-50-56-f0-72-aa     dynamique
  192.168.70.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.33.2.250 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.1.238           50-76-af-88-6c-0b     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.120.1 --- 0x13
  Adresse Internet      Adresse physique      Type
  192.168.120.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.20.1 --- 0x18
  Adresse Internet      Adresse physique      Type
  10.10.20.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.245.1 --- 0x1a
  Adresse Internet      Adresse physique      Type
  192.168.245.254       00-50-56-f5-66-9f     dynamique
  192.168.245.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.79.1 --- 0x52
  Adresse Internet      Adresse physique      Type
  192.168.79.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```


- listez les adresses MAC associées aux adresses IP que vous avez ping

Voici la liste des adresses MAC associées aux adresses IP que j'ai ping : 

**_1ère machine_**
```
Host is up (0.063s latency).
MAC Address: 84:FD:D1:F1:23:7C (Intel Corporate)
Nmap scan report for 10.33.0.250
```

**_2ère machine_**
```
Host is up (0.79s latency).
MAC Address: A0:78:17:B5:63:BB (Apple)
Nmap scan report for 10.33.3.232
```

**_3ère machine_**
```
Host is up (0.16s latency).
MAC Address: F8:5E:A0:17:91:ED (Intel Corporate)
Nmap scan report for 10.33.3.218
```


> - C. nmap

🌞Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

- réinitialiser votre conf réseau (reprenez une IP automatique, en vous déconnectant/reconnectant au réseau par exemple)

Je retourne dans l'espace ou je peux modifier mon adresse IP et je la remplace par une IP libre, une IP qui n'est pas prise sur le réseau. Pour en trouver une, je fais un **_nmap -sn -n <adresse IP du réseau>_**, et je cherche entre deux adresses IP celles qui sont libres. Par exemple ci-dessous, entre la .255 et la .232 j'ai 7 IP libres, je peux en choisir une d'entre elles.

```
Host is up (0.17s latency).
MAC Address: A0:78:17:B5:63:BB (Apple)
Nmap scan report for 10.33.3.225
Host is up (0.019s latency).
MAC Address: 34:2E:B7:49:5A:CF (Intel Corporate)
Nmap scan report for 10.33.3.232
```
J'ai choisi une adresse IP libre au hasard qui sera **_10.33.3.226_** parmis toute celles qui sont disponibles, comme on peut le voir c'est bien le cas, ce n'est pas l'adresse IP attribuée automatiquement, elle apparait bien donc cela a réussi.

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 5C-3A-45-06-7A-5F
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b1da:c56d:bcf4:d995%16(préféré)
  -> Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.226(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   IAID DHCPv6 . . . . . . . . . . . : 89930309
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-72-CB-D2-00-E0-4C-68-A2-A1
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```


- lancez un scan de ping sur le réseau YNOV




- affichez votre table ARP

On voit bien que l'adresse IP correspond à la mienne (la nouvelle, celle que je me suis attribué).

```
Interface : 10.33.3.226 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.31            fa-c5-6d-60-4b-4c     dynamique
  10.33.0.42            ce-a6-a5-8c-f3-7a     dynamique
  10.33.0.48            54-4e-90-3c-33-3f     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.99            e0-cc-f8-99-2b-27     dynamique
  10.33.0.111           d2-41-f0-dc-6a-ed     dynamique
  10.33.0.143           f0-18-98-41-11-07     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.212           48-2c-a0-c8-f1-dc     dynamique
  10.33.0.215           d8-f8-83-a6-59-2d     dynamique
  10.33.1.41            9c-fc-e8-36-7b-ee     dynamique
  10.33.1.54            30-d1-6b-26-05-63     dynamique
  10.33.1.117           3e-0f-17-bb-bc-8a     dynamique
  10.33.1.194           20-16-b9-84-86-1d     dynamique
  10.33.1.208           b2-dd-d8-d4-94-6c     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.1.246           70-66-55-68-46-a9     dynamique
  10.33.2.53            7c-04-d0-ce-8c-8a     dynamique
  10.33.2.114           40-ec-99-4c-f2-7f     dynamique
  10.33.2.149           82-2d-4a-c7-24-c4     dynamique
  10.33.2.198           24-ee-9a-e9-46-93     dynamique
  10.33.3.50            38-87-d5-d7-71-2a     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.88            4c-02-20-4b-a9-d9     dynamique
  10.33.3.120           3c-6a-a7-c3-0a-28     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.201           82-74-57-ee-b9-59     dynamique
  10.33.3.236           3e-02-9d-7d-30-8f     dynamique
  10.33.3.241           34-7d-f6-5a-20-da     dynamique
  10.33.3.243           34-7d-f6-5a-20-da     dynamique
  10.33.3.248           34-7d-f6-5a-89-99     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

> - D. Modification d'adresse IP (part 2)


🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap

J'utilise cette fois-ci l'adresse IP libre **_10.33.3.251_**

   - utilisez un ping scan sur le réseau YNOV


   - montrez moi la commande nmap et son résultat

```
PS C:\Users\anton> nmap -sn -n 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 11:27 Paris, Madrid (heure d?ÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.058s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.29s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
Nmap scan report for 10.33.0.19
Host is up (0.11s latency).
MAC Address: D4:A3:3D:C8:CA:67 (Apple)
Nmap scan report for 10.33.0.31
Host is up (0.29s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.42
Host is up (0.38s latency).
MAC Address: CE:A6:A5:8C:F3:7A (Unknown)
Nmap scan report for 10.33.0.43
Host is up (0.080s latency).
MAC Address: 2C:8D:B1:94:38:BF (Intel Corporate)
Nmap scan report for 10.33.0.57
Host is up (0.085s latency).
MAC Address: F0:18:98:8C:6F:CD (Apple)
Nmap scan report for 10.33.0.58


....
....


Host is up (0.027s latency).
MAC Address: 36:EC:48:54:F0:19 (Unknown)
Nmap scan report for 10.33.3.236
Host is up (0.081s latency).
MAC Address: 3E:02:9D:7D:30:8F (Unknown)
Nmap scan report for 10.33.3.239
Host is up (0.81s latency).
MAC Address: 38:F9:D3:AE:C3:A3 (Apple)
Nmap scan report for 10.33.3.243
Host is up (0.14s latency).
MAC Address: 34:7D:F6:5A:20:DA (Intel Corporate)
Nmap scan report for 10.33.3.248
Host is up (0.14s latency).
MAC Address: 34:7D:F6:5A:89:99 (Intel Corporate)
Nmap scan report for 10.33.3.249
Host is up (0.029s latency).
MAC Address: 58:96:1D:17:43:F5 (Intel Corporate)
Nmap scan report for 10.33.3.252
Host is up (0.052s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.052s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.051s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.226
Host is up.
Nmap done: 1024 IP addresses (142 hosts up) scanned in 28.16 seconds

```

   - configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)

J'ai configuré la gateway avec celle d'Ynov comme le montre la photo ci-dessous
   
   ![](https://i.imgur.com/ve1uwyq.png)

   
   - prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet

on utilise la commande **_ipconfig /all_** pour prouver qu'on a un accès a Internet, une passerelle définie et une IP choisie manuellement

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 5C-3A-45-06-7A-5F
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b1da:c56d:bcf4:d995%16(préféré)
  -> Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.251(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
  -> Passerelle par défaut. . . . . . . . . : 10.33.3.253
   IAID DHCPv6 . . . . . . . . . . . : 89930309
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-72-CB-D2-00-E0-4C-68-A2-A1
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
___

# II. Exploration locale en duo

Aide de Mukesh et Sacha pour cette partie

 ## 1. Modification d'adresse IP
 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

choisissez une IP qui commence par "192.168"
utilisez un /30 (que deux IP disponibles)

![](https://i.imgur.com/xKU4uO8.png)


- vérifiez à l'aide de commandes que vos changements ont pris effet

Nous voyons bien ici que notre adresse IP est comme nous voulons donc 192.168.10.2
 
 ```
 PS C:\Users\Muralee> ipconfig /all

 
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 3C-7C-3F-1A-6C-49
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::75ca:b818:94f4:af90%25(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
  
  
 ```

- utilisez ping pour tester la connectivité entre les deux machines
 
 
 La connexion est établi entre les deux ordinateur.
 ``` 
 PS C:\Users\Muralee> ping 192.168.10.1

Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.10.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
    
```
 
- Affichez et consultez votre table ARP

Nous trouvons ici la commande qui permet d'accéder à notre table ARP puis les informations concernant notre carte reseau Ethernet.

```
PS C:\Users\Muralee> arp -a

Interface : 192.168.10.2 --- 0x19
  Adresse Internet      Adresse physique      Type
  192.168.10.1          b4-a9-fc-9f-9b-46     dynamique
  192.168.10.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

```


## 4. Utilisation d'un des deux comme gateway
 
- pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu 

```
PS C:\Users\Muralee> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 22ms, Moyenne = 21ms
 

```



- utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
PS C:\Users\Muralee> tracert 192.168.10.1

Détermination de l’itinéraire vers LAPTOP-FLEOPDHT [192.168.10.1]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  LAPTOP-FLEOPDHT [192.168.10.1]

Itinéraire déterminé.
PS C:\Users\Muralee> tracert 1.1.1.1

Détermination de l’itinéraire vers 1.1.1.1 avec un maximum de 30 sauts.

  1     1 ms     *        2 ms  LAPTOP-FLEOPDHT [192.168.10.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms    12 ms     5 ms  10.33.3.253
  4     8 ms     5 ms     4 ms  10.33.10.254
  5    14 ms    10 ms    10 ms  92.103.174.137
  6    12 ms    11 ms    11 ms  92.103.120.182
  7    24 ms    22 ms    21 ms  172.19.130.117
  8    25 ms    24 ms    24 ms  46.218.128.74
  9    48 ms    22 ms    22 ms  195.42.144.143
 10    22 ms    19 ms    19 ms  1.1.1.1

Itinéraire déterminé.

```

## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de netcat (abrégé nc). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.


L'idée ici est la suivante :

l'un de vous jouera le rôle d'un serveur

l'autre sera le client qui se connecte au serveur




sur le PC serveur avec par exemple l'IP 192.168.1.1

nc.exe -l -p 8888

"netcat, écoute sur le port numéro 8888 stp"

il se passe rien ? Normal, faut attendre qu'un client se connecte



🌞 sur le PC client avec par exemple l'IP 192.168.1.2


nc.exe 192.168.1.1 8888

"netcat, connecte toi au port 8888 de la machine 192.168.1.1 stp"


une fois fait, vous pouvez taper des messages dans les deux sens

![](https://i.imgur.com/4gUtS0Y.png)


## 6. Firewall

Activer votre firewall

Mon firewall est ici activé 

![](https://i.imgur.com/8322rxC.png)


🌞 Autoriser les ping


Pour autoriser les pings, je dois aller dans **_Pare-feu Windows Defender avec fonctions avancées de sécurité_**, une fois dedans, je vais devoir créer des règles : une pour pouvoir autoriser les requêtes ICMPv4 et une deuxième pour les requêtes ICMPv6. 

Je clique sur **__Règles de trafic entrant__** puis je clique droit dessus et ajoute une nouvelle règle avec le bouton **__nouvelle règle__**

![](https://i.imgur.com/fXlQm1R.png)



Je créé donc ma règle ICMPv4 pour autoriser les pings en mettant dans "types de protocoles", **__ICMPv4__**

![](https://i.imgur.com/u9x6Q3G.png)

Puis je clique sur perso et coche la case **__requêtes d'écho__**

![](https://i.imgur.com/R1ko9ZN.png)

Je n'aurais plus qu'à lui donner un nom et ce sera presque terminé car je dois faire exactement la même chose mais en sélectionnant cette fois ci ICMPv6 au lieu de ICMPv4.


🌞 Autoriser le traffic sur le port qu'utilise nc

on parle bien d'ouverture de port TCP et/ou UDP
on ne parle PAS d'autoriser le programme nc

choisissez arbitrairement un port entre 1024 et 20000
vous utiliserez ce port pour communiquer avec netcat par groupe de 2 toujours
le firewall du PC serveur devra avoir un firewall activé et un netcat qui fonctionne

---

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

🌞Exploration du DHCP, depuis votre PC

J'utilise la commande **__ipconfig /all__** pour trouver les informations

  - afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
  
  
  
  - cette adresse a une durée de vie limitée. C'est le principe du bail DHCP (ou DHCP lease). Trouver la date d'expiration de votre bail DHCP

La date d'expiration de mon bail DHCP est le lundi 20 Septembre 2021 à 19:14:19

![](https://i.imgur.com/a4CqD3c.png)


## 2. DNS

🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

Voici l'adresse IP du serveur DNS 

![](https://i.imgur.com/sckAR3G.png)


🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

 - faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")

Voici les lookup réalisé pour faire les requêtes DNS demandées : 

 - pour google.com

![](https://i.imgur.com/g6DAJ9J.png)

L'adresse IP de Google est : 216.58.206.238

 - pour ynov.com

![](https://i.imgur.com/Tn3fd4G.png)

L'adresse IP d'ynov est : 92.243.16.143


- déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes



- faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")

Pour le lookup, cela me permettait de savoir l'IP d'un serveur par son nom, la c'est l'inverse, je vais faire un lookup de l'adresse IP pour que cela puisse me donner le nom de domaine qui lui est attribué, voici les résultats : 

  - pour l'adresse 78.74.21.21

![](https://i.imgur.com/tXw6wqh.png)

L'adresse 78.74.21.21 a donc pour nom de domaine **__host -78-74-21-21-homerum.telia.com__**

  - pour l'adresse 92.146.54.88

![](https://i.imgur.com/dM4YTCc.png)

L'adresse 92.146.54.88 a donc pour nom de domaine **__apoitiers -654-1-167-88.w92-146.abo.wanadoo.fr__**

---

## IV. Wireshark

🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

  - un ping entre vous et la passerelle

###### Ping 

![](https://i.imgur.com/PTcBATU.png)


  - un netcat entre vous et votre mate, branché en RJ45

###### Netcat 

![](https://i.imgur.com/wQFQsf3.png)


  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

###### DNS 

![](https://i.imgur.com/PaysIgM.png)
