###### tags: `RESEAU`
B2 | TP4 réseau leo (18/10/2021 - )

# RESEAU TP4 : Vers un réseau d'entreprise

## I. Dumb switch

#### 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |


#### 3. Setup topologie 1

- définissez les IPs statiques sur les deux VPCS

> IP PC1
```
PC1> ip 10.1.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1>
```

> IP PC2
```
PC2> ip 10.1.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2>
```
- ping un VPCS depuis l'autre

> ping de PC1 vers PC2
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.133 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.523 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=7.746 ms
^C
PC1>
```

> ping de PC2 vers PC1

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=4.050 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=13.759 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=4.230 ms
^C
PC2> 
```

---

## II. VLAN

#### 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

#### 3. Setup topologie 2

🌞 Adressage

- définissez les IPs statiques sur tous les VPCS

> IP PC1
```
PC1> ip 10.1.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1>
```

> IP PC2
```
PC2> ip 10.1.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2>
```

> IP PC3

```
PC3> ip 10.1.1.3 255.255.255.0
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

PC3>

```

- vérifiez avec des ping que tout le monde se ping


> ing de PC1 vers PC2
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.133 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.523 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=7.746 ms
^C
PC1>


```

> ping de PC2 vers PC1

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=4.050 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=13.759 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=4.230 ms
^C
PC2> 
```

> PC3

PC3 vers PC1

```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.082 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.830 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=12.774 ms
^C
PC3>    
```

PC3 vers PC2

```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=11.500 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.033 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=7.621 ms
^C
PC3> 
```

🌞 Configuration des VLANs

- référez-vous à la section VLAN du mémo Cisco
- déclaration des VLANs sur le switch sw1

pour définir mes VLAN je me connecte en privilégié et je fait la commande ```conf t``` pour configurer les VLAN : 

```
Switch>enable
Switch#
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#
Switch(config)#
```

```
Switch(config)#vlan 10
Switch(config-vlan)#
Switch(config-vlan)#name admins
Switch(config-vlan)#
Switch(config-vlan)#exit
Switch(config)#
Switch(config)#vlan 20
Switch(config-vlan)#
Switch(config-vlan)#name guests
Switch(config-vlan)#
Switch(config-vlan)#exit
Switch(config)#
Switch(config)#exit
Switch#
```
et ensuite je montre le résultat avec un ```show vlan``` :

```
Switch# show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active
20   guests                           active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

[...]

Switch#
```
- ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)

> config VLAN PC1

```
Switch(config)#interface GI0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
```

> config VLAN PC2

```
Switch(config)#interface GI0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#
```

> config VLAN PC3

```
Switch(config)#interface GI0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 20
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
```

- ici, tous les ports sont en mode access : ils pointent vers des clients du réseau



🌞 Vérif


- pc1 et pc2 doivent toujours pouvoir se ping

> ping PC1 vers PC2

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=1.697 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.231 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=6.881 ms
^C
PC1>
```

> ping PC2 vers PC1

```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.432 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=13.762 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=11.004 ms
^C
PC2> 
```

- pc3 ne ping plus personne

> ping de PC3 vers PC1

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3>
```

> ping de PC3 vers PC2

```
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable

PC3>
```

---

## III. Routing

#### 2. Adressage topologie 3*

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |


#### 3. Setup topologie 3

🖥️ ```VM web1.servers.tp4```, déroulez la Checklist VM Linux dessus

🌞 Adressage

- définissez les IPs statiques sur toutes les machines sauf le routeur

> IP statique PC1

```
PC1> ip 10.1.1.1 255.255.255.0
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1>

```

> IP statique PC2

```
PC2> ip 10.1.1.2 255.255.255.0
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2>
```
> IP statique adm1

```
adm1> ip 10.2.2.1 255.255.255.0
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0

adm1>
```

> IP statique web1

```
enp0s3 = 10.3.1.1
```

🌞 Configuration des VLANs

- référez-vous au mémo Cisco
- déclaration des VLANs sur le switch sw1
- - ajout des ports du switches dans le bon VLAN (voir le tableau d'adressage de la topo 2 juste au dessus)

> config VLAN PC1

```
Switch(config)#interface GI0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#exit
```

> config VLAN PC2

```
Switch(config)#interface GI0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 11
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
```

> config VLAN PC3

```
Switch(config)#interface GI0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 12
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
```

> config VLAN serveur

```
Switch(config)#interface GI0/3
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 13
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#
```

- il faudra ajouter le port qui pointe vers le routeur comme un trunk : c'est un port entre deux équipements réseau (un switch et un routeur)

```
Switch(config)#interface GI0/0
Switch(config-if)#
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#
Switch(config-if)#exit
```

🌞 Config du routeur

- attribuez ses IPs au routeur

- 3 sous-interfaces, chacune avec son IP et un VLAN associé

```
R1(config)#interface f0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#interface f0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#interface f0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
R1#
```

🌞 Vérif

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau


- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux

    - ajoutez une route par défaut sur les VPCS
    - ajoutez une route par défaut sur la machine virtuelle
    - testez des ping entre les réseaux