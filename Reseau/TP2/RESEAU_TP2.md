B2 | TP2 réseau leo (20/09/2021 - )

# RESEAU TP2

# I. ARP

## 1. Echange ARP

🌞Générer des requêtes ARP

  - effectuer un ping d'une machine à l'autre


> Voici un ping de ma première machine vers la deuxième

```
[antonin@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.11 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.710 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.657 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.698 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3084ms
rtt min/avg/max/mdev = 0.657/0.792/1.106/0.185 ms
```

> et inversement de la deuxième vers la 1ère
 
```
[antonin@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.789 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.773 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.701 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.674 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3111ms
rtt min/avg/max/mdev = 0.674/0.734/0.789/0.051 ms
```

  - observer les tables ARP des deux machines 

> table arp de ma première vm

```
[antonin@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:0b [ether] on enp0s8
? (10.2.1.12) at 08:00:27:02:56:1b [ether] on enp0s8
? (10.2.1.12) at 08:00:27:02:56:1b [ether] on enp0s8
```

> table arp de ma deuxième vm

```
[antonin@node2 ~]$ arp -a
? (10.2.1.11) at 08:00:27:73:00:cf [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:0b [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:0b [ether] on enp0s8
```

  - repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

> Ici dans ma première vm, je repère bien la présence de l'adresse MAC dans la table ARP de node2. 

```
[antonin@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
10.2.1.12                ether   08:00:27:02:56:1b   C                     enp0s8
```

> et pour l'autre machine c'est pareil 

```
[antonin@node2 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
-> 10.2.1.11                ether   08:00:27:73:00:cf   C                     enp0s8
10.2.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
```

  - prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)

- une commande pour voir la table ARP de node1

L'adresse MAC de node 1 est **__08:00:27:73:00:cf__**

```
[antonin@node2 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
10.2.1.11                ether   08:00:27:73:00:cf   C                     enp0s8
10.2.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
```

en faisant un **__ip a__**, ici on voit bien que sur notre node l'adresse MAC correspond
```
 enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
->    link/ether 08:00:27:73:00:cf brd ff:ff:ff:ff:ff:ff
```


- et une commande pour voir la MAC de node2

Comme montré ci-dessous, l'adresse MAC de node 2 est **__08:00:27:02:56:1b__**

```
[antonin@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:0b   C                     enp0s8
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
10.2.1.12                ether   08:00:27:02:56:1b   C                     enp0s8
```

en faisant un **__ip a__**, ici on voit bien que sur notre node l'adresse MAC correspond
```
enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
->    link/ether 08:00:27:02:56:1b brd ff:ff:ff:ff:ff:ff
```

## 2. Analyse de trames

🌞Analyse de trames

- utilisez la commande tcpdump pour réaliser une capture de trame

> capture de trame de ma première vm

Je vais lister toute les commandes dans l'ordre qu'il faut utiliser pour capturer la trame : 

**__sudo tcpdump -i enp0s8__** (qui va lister tout le trafic de l'interface enp0s8)

**__sudo tcpdump -i enp0s8 -c 10__** (Capturer uniquement 10 trames de l'interface enp0s8)

**__sudo tcpdump -i enp0s8 -c 10 -w mon_fichier.pcap__** (On capture aussi uniquement 10 trames de l'interface enp0s8 mais cette fois-ci en enregistrant la capture dans un fichier .pcap)

Plus qu'a faire un ping de notre node1 sur notre node2 et on devrait pouvoir récupérer la trame que voici : 

> capture de trame node1
```
[antonin@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```

> capture de trame node2

- videz vos tables ARP, sur les deux machines, puis effectuez un ping

On vide les tables ARP avec la commande **__sudo ip neigh flush all__**

> ping node1 vers node2

```
[antonin@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.706 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.650 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.645 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.736 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3089ms
rtt min/avg/max/mdev = 0.645/0.684/0.736/0.042 ms
```

> ping node2 vers node1

```
[antonin@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.518 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.655 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.610 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.602 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3082ms
rtt min/avg/max/mdev = 0.518/0.596/0.655/0.052 ms
```

- stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
- mettez en évidence les trames ARP
- écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames

  - uniquement l'ARP
  - sans doublons de trame, dans le cas où vous avez des trames en double

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP |PcsCompu_02:56:1b (08:00:27:02:56:1b)| Broadcast (ff:ff:ff:ff:ff:ff) |
| 2     | Réponse ARP |PcsCompu_73:00:cf (08:00:27:73:00:cf)| PcsCompu_02:56:1b (08:00:27:02:56:1b)   |
|       |             |                                                       |
 
---

# II. Routage

## 1. Mise en place du routage

🌞Activer le routage sur le noeud router.2.tp2

J'active le routage sur le noeud router.2.tp2 avec les commandes suivantes : 

**__sudo firewall-cmd --add-masquerade --zone=public__**


**__sudo firewall-cmd --add-masquerade --zone=public --permanent__**


🌞Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping

Je vais maintenant ajouter les routes statiques nécessaires, et on utilise cette commande pour pouvoir mettre la route : 
**__sudo nano /etc/sysconfig/network-scripts/route-enp0s8__** 

>sur la node1

```
10.2.2.1 via 10.2.1.254 dev enp0s8
```

>sur la marcel

```
10.2.1.0/24 via10.2.2.254 dev enp0s8
```

on utilise la commande **__ip r s__** pour montrer les routes

> sur la node1

```
[antonin@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.1 via 10.2.1.254 dev enp0s8 proto static metric 100
```

> sur la marcel

```
[antonin@localhost ~]$ ip r s
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```


maintenant je prouve que les deux machines peuvent ce ping

> node1

```
[antonin@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.40 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.41 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.45 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.44 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 1.397/1.423/1.450/0.042 ms
```

> marcel

```
[antonin@localhost ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.37 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.21 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.30 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.34 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 1.213/1.303/1.365/0.067 ms
```

## 2. Analyse de trames

🌞Analyse des échanges ARP

- videz les tables ARP des trois noeuds

La table ARP ce vide avec la commande **__sudo ip neigh flush all__**


- effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2

```
[antonin@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.29 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.38 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.09 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.33 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 1.086/1.522/2.290/0.458 ms
```

- regardez les tables ARP des trois noeuds

>node 1

```
[antonin@node1 ~]$ arp -a
? (10.2.1.254) at 08:00:27:30:4c:ee [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:0b [ether] on enp0s8
```

>marcel

```
[antonin@localhost ~]$ arp -a
? (10.2.2.254) at 08:00:27:e2:68:a2 [ether] on enp0s8
? (10.2.2.1) at 0a:00:27:00:00:5c [ether] on enp0s8
```

> router

```
[antonin@localhost ~]$ arp -a
? (10.2.2.12) at 08:00:27:94:32:be [ether] on enp0s9
? (10.2.1.1) at 0a:00:27:00:00:0b [ether] on enp0s8
? (10.2.1.11) at 08:00:27:73:00:cf [ether] on enp0s8
? (10.2.1.11) at 08:00:27:73:00:cf [ether] on enp0s8
```

- essayez de déduire un peu les échanges ARP qui ont eu lieu



- répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur les 3 noeuds, afin de capturer les échanges depuis les 3 points de vue

- écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames utiles pour l'échange

| ordre | type trame  | ip source   | source                      | ip destination | destination                 |
|-------|-------------|-------------|-----------------------------|----------------|-----------------------------|
| 1     | Requête ARP | x           | `node1` `08:00:27:73:00:cf` | x              | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | x           | `router` `08:00:27:30:4c:ee`| x              | `node1` `08:00:27:73:00:cf`   |
| 3     | Requête ARP | x           | `router` `08:00:27:30:4c:ee`| x              | Broadcast `FF:FF:FF:FF:FF`  |
| 4     | Réponse ARP | x           | `marcel` `08:00:27:94:32:be`| x              | `router` `08:00:27:e2:68:a2`|
| 5     | Ping        | 10.2.1.11   | `node1` `08:00:27:73:00:cf` | 10.2.2.12      | `marcel` `08:00:27:94:32:be`  |
| 6     | Pong        | 10.2.2.12   | `marcel` `08:00:27:94:32:be`| 10.2.1.11      | `node1` `08:00:27:73:00:cf`  |


## 3. Accès internet

🌞Donnez un accès internet à vos machines

- le routeur a déjà un accès internet
- ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2

  - vérifiez que vous avez accès internet avec un ping

  - le ping doit être vers une IP, PAS un nom de domaine


- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

  - vérifiez que vous avez une résolution de noms qui fonctionne avec dig

  - puis avec un ping vers un nom de domaine


🌞Analyse de trames
